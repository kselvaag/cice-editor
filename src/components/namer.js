import React from "react";
import {connect} from "react-redux";

const InnerNamerPresentational = (props)=>
  props.focus
  ? <input
      defaultValue="identifier" onBlur={()=>{
        props.onBlur();
        props.onBlurDispatch(props.defIndex);
    }} />
  : (
    <button type="button" onClick={props.onClick}>
      {props.name || "Give name"}
    </button>
  );

const mapStateToProps = (state, ownProps)=>({ });

const mapDispatchTopProps = (dispatch)=>({
  onBlurDispatch: (defIndex)=>dispatch({type: 'name', defIndex: defIndex}),
});

const InnerNamer = connect(
  mapStateToProps,
  mapDispatchTopProps
)(InnerNamerPresentational);

export class Namer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {focus: false};
  }
  onClick() {
    this.setState({focus: true});
  }
  onBlur() {
    this.setState({focus: false});

  }
  render() {
    return <InnerNamer
      focus={this.state.focus}
      onClick={()=>this.onClick()}
      onBlur={()=>this.onBlur()}
      defIndex={this.props.defIndex}
    />;
  }
}
