import React from 'react';
import {connect} from 'react-redux';

const DefsView = (props)=>{
  console.log("props: ", props);
  return <div>{props.def.render()}</div>;
};

const CodeViewPresentational = (props)=>(
  <div>
    {props.defs.map((def, i)=><DefsView def={def} key={i} />)}
    <button type="button"
      onClick={()=>props.onDefineType(props.defs.length)}>Define type</button>
    <button type="button"
      onClick={()=>props.onDefineFunction(props.defs.length)}>Define function</button>
  </div>
);

const mapStateToProps = (state, ownProps)=>({
  defs: state.defs
});

const mapDispatchTopProps = (dispatch)=>({
  onDefineType: (defIndex)=>dispatch({type: 'define', defineType: 'type', defIndex: defIndex}),
  onDefineFunction: (defIndex)=>dispatch({type: 'define', defineType: 'function', defIndex: defIndex}),
});

export const CodeView = connect(
  mapStateToProps,
  mapDispatchTopProps
)(CodeViewPresentational);
