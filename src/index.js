import React from 'react';
import {render} from 'react-dom';
import {createStore} from 'redux';
import {Provider} from 'react-redux';
import {CodeView} from './components/codeview';
import {reducer} from './reducers';

const App = ()=>(
  <div>
    <h1>CICE-Editor</h1>
    <CodeView />
  </div>
);

const store = createStore(reducer);

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('app')
);
