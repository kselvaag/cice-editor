import {defineReducer} from "./reducers/definereducer";

function combinedReducers(state, action) {
  switch(action.type) {
    case('define'):
      return Object.assign({}, state,
        {defs: defineReducer(state.defs, action.defineType, action.defIndex)});
    case('name'):
      // TODO: Implement!
      return state;
  }
  return state;
}

export function reducer(state, action) {
  if (typeof state === "undefined") {
    return {
      defs: []
    };
  } else {
    return combinedReducers(state, action);
  }
}
