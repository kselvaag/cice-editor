import React from "react";
import {CodeTreeBase} from './codetreebase';
import {Namer} from '../components/namer';

const AddConstructor = (props)=>(
  <button type="button">+</button>
);

export class DefineType extends CodeTreeBase {
  constructor(props) {
    super();
    this.name = props.name || "";
    this.defIndex = props.defIndex;
  }
  render() {
    return (
      <div>
        <Namer name={this.name} defIndex={this.defIndex}/> = <AddConstructor />
      </div>
    );
  }
}
