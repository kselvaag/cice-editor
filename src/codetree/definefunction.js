import {CodeTreeBase} from './codetreebase';

export class DefineFunction extends CodeTreeBase {
  constructor(args) {
    super(args);
  }
  render() { return "defining function"; }
}
