import {DefineType} from '../codetree/definetype';
import {DefineFunction} from '../codetree/definefunction';

export function defineReducer(defs, defineType, defIndex) {
  switch (defineType) {
    case("type"):
      return defs.concat(new DefineType({defIndex: defIndex}));
    case("function"):
      return defs.concat(new DefineFunction());
    default:
      return defs;
  }
}
